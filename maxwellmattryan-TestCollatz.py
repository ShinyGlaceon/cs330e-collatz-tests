#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_01(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_02(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertNotEqual(i, 10)
        self.assertNotEqual(j, 1)

    def test_read_03(self):
        s = "10 10000\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 10)
        self.assertEqual(j, 10000)

    # ----
    # eval
    # ----

    def test_eval_01(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_02(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_03(self):
        v = collatz_eval(1, 100)
        self.assertEqual(v, 119)

    def test_eval_04(self):
        v = collatz_eval(100, 10000)
        self.assertEqual(v, 262)

    def test_eval_05(self):
        v = collatz_eval(10000, 100000)
        self.assertEqual(v, 351)

    def test_eval_06(self):
        v = collatz_eval(900000, 999999)
        self.assertEqual(v, 507)

    # -----
    # print
    # -----

    def test_print_01(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_02(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertNotEqual(w.getvalue(), "1 10 2\n")

    def test_print_03(self):
        w = StringIO()
        collatz_print(w, 10, 10000, 262)
        self.assertEqual(w.getvalue(), "10 10000 262\n")

    # -----
    # solve
    # -----

    def test_solve_01(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_02(self):
        r = StringIO("1 1\n1 10\n10 10000\n10000 100000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n1 10 20\n10 10000 262\n10000 100000 351\n")

    def test_solve_03(self):
        r = StringIO("270 847\n299 531\n753 786\n168 384\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "270 847 171\n299 531 144\n753 786 153\n168 384 144\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
